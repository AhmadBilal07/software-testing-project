package test_selenium_junit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

@TestInstance(Lifecycle.PER_CLASS)
public class SeleniumWithJunit {
	
	private static final Logger log = Logger.getLogger(SeleniumWithJunit.class);

	
	private WebDriver driver = null;
	private String baseUrl;

	@BeforeAll
	public void testSetUp() {
		baseUrl = "https://www.youtube.com";
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\ahmad\\Downloads\\Example\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
	}

	public void Search() throws InterruptedException {
		//driver.get(baseUrl);
		driver.findElement(By.name("search_query")).sendKeys("Software Testing",Keys.RETURN);
		}
	
	
	//please Enter you google ID and Password in identifier and password respectively
	
	public void login() throws InterruptedException {
		driver.get(baseUrl);	
		driver.findElement(By.xpath("//*[@id=\"buttons\"]/ytd-button-renderer/a")).click();
		driver.findElement(By.name("identifier")).sendKeys("Email",Keys.RETURN);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.findElement(By.name("password")).sendKeys("xxxx",Keys.RETURN);
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			
		}
	
	
	//Since Junit doesn't allows us to prioritize our test cases
	// we can prioritize them by calling in an order
	@Test 		
	public void TestCasesPriority() throws InterruptedException
	{
		BasicConfigurator.configure();
		
		try {
			
			log.info("Testing Started");
			log.info("Login Scenerio Under Testing");
			login();
			log.info("Search Scenario Under Testing");
			Search();
			Thread.sleep(10000);
			System.out.println("Both Test Scenarios Executed Successfully" ); 
		
				
			
		} catch (Exception e) {
			log.info("Some error occurred");
			log.error(e, e);
		}
	
	}
	
	@AfterAll
	public void tearDown() {
		driver.close();
		}
}